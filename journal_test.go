package main

import (
	"bytes"
	"strings"
	"testing"
	"time"
)

const data = `2018-07-20 09:00: This is the subject line.
Here is an entry with a @tag.

2018-07-21 10:00: subject take 2. This time, the @entry and the @subject are on the same line.`

const formattedData = `2018-07-20 09:00: This is the subject line.
Here is an entry with a @tag.

2018-07-21 10:00: subject take 2.
This time, the @entry and the @subject are on the same line.`

var appended = strings.Join([]string{
	formattedData,
	"\n\n",
	`2018-07-22 11:00: subject take 3.
here's a body line`,
}, "")

var e1 = &Entry{
	Title: "This is the subject line.",
	Body:  "Here is an entry with a @tag.",
	Tags:  []string{"@tag"},
	Time:  time.Date(2018, 7, 20, 9, 0, 0, 0, time.UTC),
}

var e2 = &Entry{
	Title: "subject take 2.",
	Body:  "This time, the @entry and the @subject are on the same line.",
	Tags:  []string{"@entry", "@subject"},
	Time:  time.Date(2018, 7, 21, 10, 0, 0, 0, time.UTC),
}

var e3 = &Entry{
	Title: "subject take 3.",
	Body:  "here's a body line",
	Time:  time.Date(2018, 7, 22, 11, 0, 0, 0, time.UTC),
}

var j = Journal([]*Entry{e1, e2})

func TestLoadJournalFromFile(t *testing.T) {
	loaded, err := loadJournalFromFile(strings.NewReader(data))
	ok(t, err)
	equals(t, j, loaded)
}

func TestWriteJournalToFile(t *testing.T) {
	buf := bytes.NewBuffer([]byte{})
	j.writeJournalToFile(buf)
	equals(t, formattedData, string(buf.Bytes()))
}

func TestAppendEntryToJournal(t *testing.T) {
	loaded, err := loadJournalFromFile(strings.NewReader(formattedData))
	ok(t, err)
	j = append(loaded, e3)
	buf := bytes.NewBuffer([]byte{})
	j.writeJournalToFile(buf)
	equals(t, appended, string(buf.Bytes()))
}
