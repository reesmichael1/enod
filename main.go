package main

import (
	"flag"
	"fmt"
	// "io/ioutil"
	"log"
	"os"
	"os/user"
	"path"
	"strings"
)

func getJournalPath() (string, error) {
	u, err := user.Current()
	if err != nil {
		return "", err
	}

	enodDir := path.Join(u.HomeDir, ".enod")
	if _, err := os.Stat(enodDir); os.IsNotExist(err) {
		os.Mkdir(enodDir, 0755)
	}
	enodPath := path.Join(enodDir, "enod")
	if _, err := os.Stat(enodDir); os.IsNotExist(err) {
		os.Create(enodPath)
	}

	return enodPath, nil
}

func main() {
	p, err := getJournalPath()
	if err != nil {
		log.Fatal(err)
	}
	file, err := os.OpenFile(p, os.O_CREATE|os.O_RDWR, 0644)
	defer file.Close()

	if err != nil {
		log.Fatal(err)
	}
	j, err := loadJournalFromFile(file)
	if err != nil {
		log.Fatal(err)
	}
	flag.Parse()
	input := flag.Args()
	var e *Entry
	if len(input) > 0 {
		entry := strings.Join(input, " ")
		e, err = LoadEntryFromInput(entry)

		if err != nil {
			log.Fatal(err)
		}

	} else {
		log.Printf("no entry provided, opening editor")
		i, err := LoadEntryFromEditor()
		if err != nil {
			log.Fatal(err)
		}

		if i == "" {
			log.Print("no entry could be parsed from the editor")
			fmt.Println(j)
			os.Exit(0)
		}

		e, err = LoadEntryFromInput(i)

		if err != nil {
			log.Fatal(err)
		}

		if e == nil {
			log.Print("no entry could be parsed from the editor")
			fmt.Println(j)
			os.Exit(0)
		}
	}

	// Not my favorite way of avoiding duplicate entries, but it'll do for now
	file.Truncate(0)
	file.Seek(0, 0)

	j = append(j, e)
	j.writeJournalToFile(file)

	log.Printf("added entry for %s to %s\n", e.Time, p)
}
