package main

import (
	"testing"
	"time"
)

func TestLoadEntryFromInputWithTime(t *testing.T) {
	r, err := LoadEntryFromInput("tomorrow: entry")
	ok(t, err)

	exp := &Entry{Title: "entry", Time: truncateTime(time.Now().AddDate(0, 0, 1))}
	equals(t, exp, r)
}

func TestLoadEntryFromInputWithoutTime(t *testing.T) {
	r, err := LoadEntryFromInput("entry without a time")
	ok(t, err)

	exp := &Entry{Title: "entry without a time", Time: truncateTime(time.Now())}
	equals(t, exp, r)
}

func TestLoadEntryFromInputWithoutTimeAndWithColon(t *testing.T) {
	r, err := LoadEntryFromInput("entry with a list: a b c")
	ok(t, err)

	exp := &Entry{Title: "entry with a list: a b c", Time: truncateTime(time.Now())}
	equals(t, exp, r)
}

func TestLoadEntryFromInputWithMultipleColons(t *testing.T) {
	r, err := LoadEntryFromInput("tomorrow: entry with a list: a b c")
	ok(t, err)

	exp := &Entry{Title: "entry with a list: a b c", Time: truncateTime(time.Now().AddDate(0, 0, 1))}
	equals(t, exp, r)
}

func TestLoadEntryWithTitleAndBody(t *testing.T) {
	r, err := LoadEntryFromInput("today: this is the subject. this is the body")
	ok(t, err)

	exp := &Entry{Title: "this is the subject.", Body: "this is the body", Time: truncateTime(time.Now())}
	equals(t, exp, r)
}

func TestLoadEntryWithTags(t *testing.T) {
	r, err := LoadEntryFromInput("today: subject. body with @tag1 and @tag2.")
	ok(t, err)

	exp := &Entry{Title: "subject.", Body: "body with @tag1 and @tag2.", Tags: []string{"@tag1", "@tag2"}, Time: truncateTime(time.Now())}
	equals(t, exp, r)
}

func TestIsEntryFavorite(t *testing.T) {
	r, err := LoadEntryFromInput("today: fell in love*")
	ok(t, err)

	exp := &Entry{Title: "fell in love*", IsFavorite: true, Time: truncateTime(time.Now())}
	equals(t, exp, r)
}

func TestLoadEntryWithExplicitTime(t *testing.T) {
	r, err := LoadEntryFromInput("2018-07-20 18:00: subject.")
	ok(t, err)

	exp := &Entry{Title: "subject", Time: time.Date(2018, 7, 20, 18, 0, 0, 0, time.UTC)}
	equals(t, exp, r)
}
