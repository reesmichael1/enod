package main

import (
	"fmt"
	"strings"
	"time"
)

// Entry holds the information given by the user
// to place in a journal entry
type Entry struct {
	Title      string
	Body       string
	Tags       []string
	IsFavorite bool
	Time       time.Time
}

func (e Entry) String() string {
	return fmt.Sprintf("%s: %s\n%s", e.Time.Format("2006-01-02 15:04"), strings.TrimSpace(e.Title), strings.TrimSpace(e.Body))
}
