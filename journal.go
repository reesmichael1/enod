package main

import (
	"bytes"
	"io"
	"io/ioutil"
)

// Journal holds all of the entries that compose the journal
type Journal []*Entry

func loadJournalFromFile(f io.Reader) (Journal, error) {
	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	if len(b) == 0 {
		return Journal([]*Entry{}), nil
	}
	chunks := bytes.Split(b, []byte("\n\n"))
	entries := []*Entry{}
	for _, chunk := range chunks {
		e, err := LoadEntryFromInput(string(chunk))
		if err != nil {
			return nil, err
		}
		entries = append(entries, e)
	}

	return Journal(entries), nil
}

func (j Journal) writeJournalToFile(f io.Writer) {
	for ix, entry := range j {
		f.Write([]byte(entry.String()))
		// Add newlines between entries except for the last one
		if ix != len(j)-1 {
			f.Write([]byte("\n\n"))
		}
	}
}
