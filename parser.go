package main

import (
	"regexp"
	"strings"
	"time"

	"github.com/olebedev/when"
	"github.com/olebedev/when/rules/common"
	"github.com/olebedev/when/rules/en"
)

func delimiters(r rune) bool {
	return r == '.' || r == '!' || r == '?'
}

func loadEntryTitleFromInput(input string, foundTime bool) string {
	var text string
	if foundTime {
		text = strings.SplitN(input, ": ", 2)[1]
	} else {
		text = input
	}

	// TODO: use regexes to handle all the possible cases correctly
	s := strings.FieldsFunc(text, delimiters)

	if len(s) == 1 {
		return s[0]
	}

	return s[0] + "."
}

func truncateTime(t time.Time) time.Time {
	return t.Truncate(time.Minute)
}

// NoTimeGivenError is returned whenever a time is not found in the given input
type NoTimeGivenError struct{}

func (e NoTimeGivenError) Error() string {
	return "no time given"
}

func loadTimeFromInput(input string) (time.Time, error) {
	w := when.New(nil)
	w.Add(en.All...)
	w.Add(common.All...)

	s := strings.Split(input, ": ")

	if len(s) < 2 {
		return truncateTime(time.Now()), NoTimeGivenError{}
	}

	// simple check for YYYY-MM-DD HH:MM format (not caught by when)
	t, err := time.Parse("2006-01-02 15:04", s[0])
	if err == nil {
		return t.In(time.UTC), nil
	}

	r, err := w.Parse(s[0], time.Now())
	if err != nil {
		return truncateTime(time.Time{}), err
	}

	if r == nil {
		return truncateTime(time.Now()), NoTimeGivenError{}
	}

	return truncateTime(r.Time), nil
}

func loadEntryBodyFromInput(input string) string {
	sentences := strings.Split(input, ".")
	stripped := []string{}
	for _, s := range sentences[1:] {
		stripped = append(stripped, strings.TrimSpace(s))
	}
	return strings.Join(stripped, ".")
}

func loadTagsFromInput(input string) []string {
	re := regexp.MustCompile("@([a-zA-Z0-9]+)")
	return re.FindAllString(input, -1)
}

func loadIsFavoriteFromInput(input string) bool {
	return strings.Contains(input, "*")
}

// LoadEntryFromInput parses the full entry from a given input string
func LoadEntryFromInput(input string) (*Entry, error) {
	t, err := loadTimeFromInput(input)
	var title string
	if err != nil {
		switch err.(type) {
		case NoTimeGivenError:
			title = loadEntryTitleFromInput(input, false)
		default:
			return nil, err
		}
	} else {
		title = loadEntryTitleFromInput(input, true)
	}

	return &Entry{
		Title:      title,
		Body:       loadEntryBodyFromInput(input),
		Time:       t,
		Tags:       loadTagsFromInput(input),
		IsFavorite: loadIsFavoriteFromInput(input),
	}, nil
}
