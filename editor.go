package main

import (
	"bufio"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

// LoadEntryFromEditor loads text for a journal entry from the
// system editor, or vim if no editor is set
func LoadEntryFromEditor() (string, error) {
	fpath, err := ioutil.TempFile("", "")
	if err != nil {
		return "", err
	}

	ed := os.Getenv("EDITOR")
	if ed == "" {
		ed = "vim"
	}

	cmd := exec.Command(ed, fpath.Name())

	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Start()
	if err != nil {
		return "", err
	}
	err = cmd.Wait()
	if err != nil {
		return "", err
	}

	scanner := bufio.NewScanner(fpath)
	var r strings.Builder
	for scanner.Scan() {
		r.WriteString(scanner.Text())
	}

	return r.String(), nil
}
